import * as Knex from 'knex';

export class TrainingService{
    constructor(private knex:Knex){

    }

    getTraining(){
        return this.knex.select("*").from('training');
    }

    async addTraining(body:any){
      
        return this.knex.table("").insert({
            credit:body.credit  
        });
      }
    

    updateTraining(id:number,body:any){
        return this.knex.table("training")
                .update(body).where("id",id);
    }

    delete(id:number){

        return this.knex.table("training").where("id",id).delete();
    }

}
