

import * as Knex from "knex";

exports.seed = async function (knex: Knex): Promise<any> {
    await knex("redemption").del();
    await knex("user_training_sessions").del();
    await knex("gifts").del();
    await knex("training").del();
    await knex("users").del();
    

    const result1 =await knex("training").insert([
        { credit:"50" },
        { credit:"15" },
        { credit:"10" }
    ]).returning("id");
    const[firstcredit,secondcredit,thirdcredit] = result1;

    const result =await knex("users").insert([
        { username: "alex", password: "1111",user_code:1111,isAdmin:"1",credit:"100"},
        { username: "gordon", password: "1111" ,user_code:2222,isAdmin:"1",credit:"100"},
        { username: "michael", password: "1111",user_code:3333,isAdmin:"0",credit:"100" },

    ]).returning("id");

    const[alex,gordon,michael] = result;

    const result2 =await knex("gifts").insert([
        {giftname: "AV", point: 15,image_path:"",stock:10},
        { giftname: "MV", point: 25,image_path:"",stock:9},
    ]).returning("id");;

    const[AV,MV] = result2;
    await knex("user_training_sessions").insert([
        { user_id: alex, training_id: firstcredit, credit_gain:10,time:"02:30:15",finished_at:"2018/12/21 21:23:20",RPM_avg:310},
        { user_id: alex, training_id: firstcredit , credit_gain:10,time:"02:30:15",finished_at:"2018/12/21 21:23:20",RPM_avg:310},
        { user_id: michael, training_id: secondcredit , credit_gain:10,time:"02:30:15",finished_at:"2018/12/21 21:23:20",RPM_avg:310},
        { user_id: gordon, training_id: thirdcredit , credit_gain:10,time:"02:30:15",finished_at:"2018/12/21 21:23:20",RPM_avg:310},
        { user_id: gordon, training_id: thirdcredit , credit_gain:10,time:"02:30:15",finished_at:"2018/12/21 21:23:20",RPM_avg:310},
    ]);

    await knex("redemption").insert([
        { user_id: alex, gift_id: AV ,trading_point:10},
        { user_id: alex, gift_id: AV ,trading_point:20},
        { user_id: alex, gift_id: AV ,trading_point:15},
        { user_id: gordon, gift_id: MV ,trading_point:10}, 
        { user_id: gordon, gift_id: MV ,trading_point:25}, 
       
    ]);

    

};
