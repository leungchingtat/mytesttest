export interface EventData {
  id: number;
  name: string;
  created: string;
  attendees: string[];
  dates: EventDate[];
  venues: EventVenue[];
}

export interface EventDate {
  date: string;
  votes: number;
}

export interface EventVenue {
  name: string;
  votes: number;
}