let clock;

$(document).ready(function () {
    let clock;
    
    const targetDate = new Date("2019-03-20")
    const currentDate = new Date()
        
    
    clock = $('.clock').FlipClock({
        clockFace: 'DailyCounter',
        autoStart: false,
        callbacks: {
            stop: function () {
                $('.message').html('The clock has stopped!')
            }
        }
    });
    console.log(targetDate - currentDate);
    // clock.setTime(targetDate.getTime()/1000);
    clock.setTime((new Date("2019-Mar-03") - new Date()) /1000);
    clock.setCountdown(true);
    clock.start();

});
