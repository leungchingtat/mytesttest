window.onload = () => {
    console.log('on load');
  
    fetch('/users/me', {
      method: 'GET',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
  
     
    }).then(res => {
      return res.json();
    
    }).then(res=>{
      document.getElementById("university").value = res.university;
      document.getElementById("year").value = res.year;
      document.getElementById("address").value = res.address;
      document.getElementById("addressDetail").value = res.addressDetail;
      document.getElementById("subject").value = res.subject;
      document.getElementById("introduction").value = res.introduction;
      document.getElementById("tel").value = res.tel;
      document.getElementById("email").value = res.email;
    })
  
  
    const createForm = document.querySelector('#create-form') // grab the form 
      .addEventListener('submit', function (event) { // add a listener upon submission
        event.preventDefault();        // ^^^ given by DOM
        // ^^^^ stop the default behaviour (submission in foreground)
  
        // Serialize the Form afterwards
        // const form = this; // the `this` will be converted to the element (which is form)
        const form = event.currentTarget; // event.currentTarget is also the element triggering the event
        const formData = {}; // object
        for (let input of form) { // treat the form like an array
          if (!['submit', 'reset'].includes(input.type)) {
            formData[input.name] = input.value; // assign value to the object
          }
        }
        console.log(formData);
  
        fetch('/gifts/', {
          method: 'POST',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify(formData)
        }).then(res => {
          return res.json();
        }).then(body => {
          console.log(body);
          if (body.result == 'success') {
            //  vvvv HTML/DOM object given by the browser
            alert('Done');
            fetch('/gifts/', {
              method: 'GET',
              headers: {
                "Content-Type": "application/json; charset=utf-8",
              },
  
            }).then(res => {
              return res.json();
            }).then(body => {
              const giftsList = document.querySelector('#gifts-main');
              console.log('the element is', giftsList);
  
              const giftsListHTML = GiftList(body);
              console.log(giftsListHTML);
  
              giftsList.innerHTML = giftsListHTML;
              data.gifts = body;
  
  
              console.log(body);
              if (body.result == 'success') {
                //  vvvv HTML/DOM object given by the browser
                alert('WE have below user '); // redirect the browser to other location
                //       ^^^ magic property that when you change it, the browser will jump to that URL
              }
            });
            // redirect the browser to other location
            //       ^^^ magic property that when you change it, the browser will jump to that URL
          } else {
            alert('Wrong username or password :P');
          }
        });
      })
  
  
    const changeForm = document.querySelector('#change-form') // grab the form 
      .addEventListener('submit', function (event) { // add a listener upon submission
        event.preventDefault();        // ^^^ given by DOM
        // ^^^^ stop the default behaviour (submission in foreground)
  
        // Serialize the Form afterwards
        // const form = this; // the `this` will be converted to the element (which is form)
        const form = event.currentTarget; // event.currentTarget is also the element triggering the event
        const formData = {}; // object
        for (let input of form) { // treat the form like an array
          if (!['submit', 'reset'].includes(input.type)) {
            formData[input.name] = input.value; // assign value to the object
          }
        }
        console.log("formdata",formData);
  
        fetch('/transaction/' + formData.id, {
          method: 'PUT',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
  
          body: JSON.stringify(formData)
        }).then(res => {
          return res.json();
        }).then(body => {
          console.log(body);
          if (body.result == 'success') {
            //  vvvv HTML/DOM object given by the browser
            alert('Done');
            fetch('/gifts/', {
              method: 'GET',
              headers: {
                "Content-Type": "application/json; charset=utf-8",
              },
  
            }).then(res => {
              return res.json();
            }).then(body => {
              const giftsList = document.querySelector('#gifts-main');
              console.log('the element is', giftsList);
  
              const giftsListHTML = GiftList(body);
              console.log(giftsListHTML);
  
              giftsList.innerHTML = giftsListHTML;
              data.gifts = body;
  
  
              console.log(body);
              if (body.result == 'success') {
                //  vvvv HTML/DOM object given by the browser
                alert('WE have below user '); // redirect the browser to other location
                //       ^^^ magic property that when you change it, the browser will jump to that URL
              }
            }); // redirect the browser to other location
            //       ^^^ magic property that when you change it, the browser will jump to that URL
          } else {
            alert('Wrong username or password :P');
          }
        });
      })
  
  
    const deleteForm = document.querySelector('#delete-form') // grab the form 
      .addEventListener('submit', function (event) { // add a listener upon submission
        event.preventDefault();        // ^^^ given by DOM
        // ^^^^ stop the default behaviour (submission in foreground)
  
        // Serialize the Form afterwards
        // const form = this; // the `this` will be converted to the element (which is form)
        const form = event.currentTarget; // event.currentTarget is also the element triggering the event
        const formData = {}; // object
        for (let input of form) { // treat the form like an array
          if (!['submit', 'reset'].includes(input.type)) {
            formData[input.name] = input.value; // assign value to the object
          }
        }
        console.log(formData);
  
        fetch('/gifts/' + formData.giftname, {
          method: 'Delete',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify(formData)
        }).then(res => {
          return res.json();
        }).then(body => {
          console.log(body);
          if (body.result == 'success') {
            //  vvvv HTML/DOM object given by the browser
            alert('Done');
            fetch('/gifts/', {
              method: 'GET',
              headers: {
                "Content-Type": "application/json; charset=utf-8",
              },
  
            }).then(res => {
              return res.json();
            }).then(body => {
              const giftsList = document.querySelector('#gifts-main');
              console.log('the element is', giftsList);
  
              const giftsListHTML = GiftList(body);
              console.log(giftsListHTML);
  
              giftsList.innerHTML = giftsListHTML;
              data.gifts = body;
  
  
              console.log(body);
              if (body.result == 'success') {
                //  vvvv HTML/DOM object given by the browser
                alert('WE have below user '); // redirect the browser to other location
                //       ^^^ magic property that when you change it, the browser will jump to that URL
              }
            }); // redirect the browser to other location
            //       ^^^ magic property that when you change it, the browser will jump to that URL
          } else {
            alert('Wrong username or password :P');
          }
        });
      })
  
    const showForm = document.querySelector('#show-form') // grab the form 
      .addEventListener("click", function (event) { // add a listener upon submission
        event.preventDefault();        // ^^^ given by DOM
        // ^^^^ stop the default behaviour (submission in foreground)
  
        // Serialize the Form afterwards
        // const form = this; // the `this` will be converted to the element (which is form)
  
  
        fetch('/gifts/', {
          method: 'GET',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
  
        }).then(res => {
          return res.json();
        }).then(body => {
          const giftsList = document.querySelector('#gifts-main');
          console.log('the element is', giftsList);
  
          const giftsListHTML = GiftList(body);
          console.log(giftsListHTML);
  
          giftsList.innerHTML = giftsListHTML;
          data.gifts = body;
  
  
          console.log(body);
          if (body.result == 'success') {
            //  vvvv HTML/DOM object given by the browser
            alert('WE have below user '); // redirect the browser to other location
            //       ^^^ magic property that when you change it, the browser will jump to that URL
          }
        });
      })
  
  };
  
  
  const data = {
    gifts: []
  }
  
  async function populateData() {
    const res = await fetch('/gifts/')
    const users = await res.json();
    console.log('WE have below user', gifts);
  
    const giftsList = document.querySelector('#gifts-main');
    console.log('the element is', giftsList);
  
    const giftsListHTML = GiftsList(gifts);
    console.log(giftsListHTML);
  
    giftsList.innerHTML = giftsListHTML;
    data.gifts = gifts; // <<<<< store fetched data
  }
  
  function GiftList(gifts) {
    return `
        <ul id="gift-list">
            ${
                gifts.map((gift) => {
        return Gift(gift)
      }).join('')
      }
        </ul>
    `
  }
  
  function deleteGift(giftName) {
    const gift = data.gifts.find((gift) => gift.giftname == giftName);
    fetch('/gifts/' + gift.giftname, {
      method: 'Delete',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(gift)
    }).then(res => {
      return res.json();
    }).then(body => {
      console.log(body);
      if (body.result == 'success') {
        //  vvvv HTML/DOM object given by the browser
        alert('Done');
        fetch('/gifts/', {
          method: 'GET',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
  
        }).then(res => {
          return res.json();
        }).then(body => {
          const giftsList = document.querySelector('#gifts-main');
          console.log('the element is', giftsList);
  
          const giftsListHTML = GiftList(body);
          console.log(giftsListHTML);
  
          giftsList.innerHTML = giftsListHTML;
          data.gifts = body;
  
  
          console.log(body);
          if (body.result == 'success') {
            //  vvvv HTML/DOM object given by the browser
            alert('WE have below user '); // redirect the browser to other location
            //       ^^^ magic property that when you change it, the browser will jump to that URL
          }
        });// redirect the browser to other location
        //       ^^^ magic property that when you change it, the browser will jump to that URL
      } else {
        alert('Wrong username or password :P');
      }
  
    })
  }
  function editGift(giftName) {
    const gift = data.gifts.find((gift) => gift.giftname == giftName);
    populateForm(gift);
  }
  
  function populateForm(gift) {
    console.log('populateForm get an object:', gift);
  
    const giftForm = document.querySelector('#change-form');
  
    for (let key of Object.keys(gift)) {
      console.log('looping key', key);
      giftForm.querySelector(`[name=${key}]`).value = gift[key]; // apple[key], apple.id, apple.breed, apple.weight ...
    }
  }
  
  
  
  
  function Gift(gift) {
  
    let information = '<li class="gift-item">'
  
    for (let key of Object.keys(gift)) {
      information = information + `
          <div> ${key} : ${gift[key]}</div> `
    }
    information = information + `<i class="fa fa-edit" onclick="editGift('${gift.giftname}')"></i>`
    information = information +`<i class="fa fa-trash" onclick="deleteGift('${gift.giftname}')"></i>`
    information = information + '</li>'
  
  
    return information
  }
  