import * as Knex from 'knex';

export class UsersService{
    constructor(private knex:Knex){

    }

     getUsers(){
        return this.knex.select("*").from('users');
    }

    async addUser(body:any){
      const user = await this.knex.select("username")
        .from("users")
        .where("username", body.username);
      if (user) {
        throw new Error("Your name has been used by someone else");
      } else {
        return this.knex.table("users").insert({
            username: body.username,
            password:body.password,
            user_code:body.user_code,
            level: body.level,
            isAdmin:body.isAdmin,
            credit:body.credit
            
        });
      }
    }

     updateStudent(id:number,body:any){
        return this.knex.table("users")
                .update(body).where("id",id);
    }

    async deleteStudent(id:number){
        return this.knex.table("users").where("id",id).delete();
    }

}
