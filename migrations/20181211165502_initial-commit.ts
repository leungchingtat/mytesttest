import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
  await knex.schema.createTable('users',(table)=>{
      table.increments();
      table.string("username").unique();
      table.integer("user_code").unique();
      table.string("password");
      table.string("isAdmin");
      table.integer("credit").unsigned();;
      table.timestamps(false, true);
  });

  await knex.schema.createTable('training',(table)=>{
      table.increments();
      table.integer("credit").unsigned();;
      table.timestamps(false, true);
  });

  await knex.schema.createTable('user_training_sessions',(table)=>{
      table.increments();
      table.integer("user_id").unsigned();
      table.foreign("user_id").references("users.id");
      table.integer("training_id").unsigned();
      table.foreign("training_id").references("training.id");
      table.integer("credit_gain");
      table.time('time');
      table.timestamp('finished_at');
      table.integer("RPM_avg");
      table.timestamps(false, true);
  });

  await knex.schema.createTable('gifts',(table)=>{
      table.increments();
      table.string("giftname");
      table.integer("point").unsigned();
      table.string("image_path");
      table.integer("stock").unsigned();
      table.timestamps(false, true);
  });

  await knex.schema.createTable('redemption',(table)=>{
    table.increments();
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("users.id");
    table.integer("gift_id").unsigned();
    table.foreign("gift_id").references("gifts.id");
    table.integer("trading_point").unsigned();
    table.timestamps(false, true);
  });

  

};

exports.down = async function (knex: Knex): Promise<any> {
  await knex.schema.dropTableIfExists('redemption');
  await knex.schema.dropTableIfExists('user_training_sessions');
  await knex.schema.dropTableIfExists('venues');
  
  await knex.schema.dropTableIfExists('training');
  await knex.schema.dropTableIfExists('users');
};
