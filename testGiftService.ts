import * as Knex from 'knex';

export class GiftsService{
    constructor(private knex:Knex){

    }

    getGifts(){
        return this.knex.select("*").from('gifts');
    }

    async addGift(body:any,uploadedPath?:string){
      const gift = await this.knex.select("gift_name")
        .from("gifts")
        .where("gift_name", body.giftname);
      if (gift) {
        throw new Error("Your name has been used by someone else");
      } else {
        return this.knex.table("gifts").insert({
            giftname: body.giftname,
            point:body.point,
            image_path:uploadedPath,
            level: body.level,
            isAdmin:body.isAdmin,
            credit:body.credit
            
        });
      }
    }

    updateGift(id:number,body:any,uploadedPath?:string){
        if (uploadedPath){
        body.image_path=uploadedPath;}
        return this.knex.table("gifts")
                .update(body).where("id",id);
    }

    deleteGift(id:number){
        return this.knex.table("gifts").where("id",id).delete();
    }

}
