import * as Knex from 'knex';
import * as moment from 'moment';
import { EventData } from './models';

export class EventService {
  constructor(private knex:Knex) {

  }

  getAllEvents = async (): Promise<EventData[]> => {
    const eventData:EventData[] = await this.knex
                          .select('events.id', 'events.name', {created: 'users.username'})
                          .from('events')
                          .join("users", "events.organizer_id", "users.id");

    for (const event of eventData) {
      event.attendees = (await this.knex
        .select('users.username')
        .from('attendees')
        .where('event_id', event.id)
        .join("users", "attendees.user_id", "users.id")
      ).map((user:any) => user.username);

      const rawEventDate = (await this.knex
        .select('event_dates.id', 'event_dates.date', this.knex.raw('COUNT(event_date_id)'))
        .from('event_dates')
        .leftJoin("attendees", "event_dates.id", "attendees.event_date_id")
        .where('event_dates.event_id', event.id)
        .groupBy('event_dates.id')
      );

      event.dates = rawEventDate.map((date: any) => ({
        date: moment(date.date).format('YYYY/MM/DD'),
        votes: date.count
      }));

      const rawEventVenue = (await this.knex
        .select('venues.id', 'venues.name', this.knex.raw('attendees.event_venue_id'))
        .from('event_venues')
        .leftJoin("attendees", "event_venues.id", "attendees.event_venue_id")
        .join('venues', 'event_venues.venue_id', 'venues.id')
        .where('event_venues.event_id', event.id)
        // .groupBy('venues.id')
      );

      event.venues = rawEventVenue.map((venue: any) => ({
        venue: venue.name,
        votes: venue.count
      }));
    };
    return eventData;
  }
}