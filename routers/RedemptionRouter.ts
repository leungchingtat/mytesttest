import * as express from 'express';



import { RedemptionService } from '../services/RedemptionService';



export class RedemptionRouter {

    

    constructor(private redemptionService:RedemptionService) {
     
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const usersRedemption = await this.redemptionService.getAllRedemption();
                res.json(usersRedemption);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.redemptionService.creatOneRedemption(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
      
        return router;
    }
}