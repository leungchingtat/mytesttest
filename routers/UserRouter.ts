import * as express from 'express';



import { UserService } from '../services/UserService';



export class UserRouter {

    

    constructor(private userService:UserService) {
     
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const users = await this.userService.getAllUsers();
                res.json(users);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.userService.createOneUser(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
        router.put('/:username', async (req, res) => {
            console.log("here")
            try {
                await this.userService.changeProfile( req.body);
                
                res.json({result: 'success'});
              } catch (err) {
                res.status(404).json({ result: err.message });
              }
            
        });
        
        
      
        
        
        router.delete('/:username', async (req, res) => {
            try {
                await this.userService.delete(req.params.username);
                res.json({result: 'success'});
              } catch (err) {
                  
                res.status(404).json({ result: err.message });
              }



        });
        return router;
    }
}