import * as express from 'express';



import { User_Training_SessionsService } from '../services/user_training_sessionsService';



export class User_Training_SessionsRouter {

    

    constructor(private user_training_sessionsService:User_Training_SessionsService) {
     
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const userstraining = await this.user_training_sessionsService.getAllUserTrainingSession();
                res.json(userstraining);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.user_training_sessionsService.creatOneTraining(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
      
        return router;
    }
}