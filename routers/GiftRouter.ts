import * as express from 'express';



import { GiftService } from '../services/GiftService';



export class GiftRouter {

    

    constructor(private giftService:GiftService) {
     
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const gifts = await this.giftService.getGifts();
                res.json(gifts);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.giftService.addGift(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
        router.put('/:giftname', async (req, res) => {
            console.log("here")
            try {
                await this.giftService.updateGift( req.body);
                
                res.json({result: 'success'});
              } catch (err) {
                res.status(404).json({ result: err.message });
              }
            
        });
        
        
      
        
        
        router.delete('/:giftname', async (req, res) => {
            try {
                await this.giftService.delete(req.params.giftname);
                res.json({result: 'success'});
              } catch (err) {
                  
                res.status(404).json({ result: err.message });
              }



        });
        return router;
    }
}