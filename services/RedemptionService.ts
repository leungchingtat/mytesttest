import * as Knex from 'knex';

export class RedemptionService{
    constructor(private knex:Knex){
    }

    getAllRedemption(){
        return this.knex.select("*")
        .from('users')
        .innerJoin("redemption", "redemption.user_id", "users.id")
        .innerJoin("gifts", "gifts.id", "redemption.gift_id");
    }

    async creatOneRedemption(body:any){
        return this.knex.table("redemption").insert({
            user_id: body.user_id,
            gift_id:body.gift_id,
            trading_point:body.trading_point
          
        });

    }



}