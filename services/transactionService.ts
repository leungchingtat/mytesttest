import * as Knex from 'knex';

export class TransactionService {
    constructor(private knex: Knex) {

    }

    TradeingProcess(userId: number, giftId: number) {
        console.log("1", userId,giftId)
        this.knex.transaction(async (trx) => {
            const user = await trx("users").where("id", userId).first();
            console.log("2", user);
            if (user.credit < 0) {
                return false; // no more user credit
            }

            const gift = await trx("gifts").where("id", giftId).first();
            console.log("3", gift.stock);
            if (gift.stock < 0) {
                return false; // no more stock
            }

            // TODO: check update result
            await trx("gifts").update("stock", gift.stock - 1).where("id", giftId);
            const remainingCredit = user.credit - gift.point;
            console.log("4", remainingCredit);

            if (remainingCredit < 0) {
                return false; // no more remaining credit
                
            }

            // TODO: check update result
            await trx("users").update("credit", remainingCredit).where("id", userId);
            // TODO: check update result
            await trx("redemption").insert({ user_id: userId, gift_id: giftId, trading_point: gift.point })
  
            return true;
        })
    }
    // getAllUserTrainingSession(userId:number, giftId:number){
    //    this.knex.transaction(async (trx) => {
    //        const user = await trx("users").where("id", userId);
    //        if (user.credit > 0) {
    //             const gift = await trx("gifts").where("id", giftId);
    //             if (gift.stock > 0) {
    //                 await trx("gifts").update("stock", gift.stock - 1).where("id", giftId);
    //                 // TODO: check update result
    //                 await trx("redemption").insert({ user_id: userId, gift_id: giftId, point: gift.point })
    //             } else {
    //                 return false;
    //             }
    //        } else {
    //            return false;
    //        }
    //    })
    //     return this.knex.select("*").from('users').innerJoin("user_training_sessions", "user_training_sessions.user_id", "users.id");
    // }

    async creatOneTraining(body: any) {
        return this.knex.table("user_training_sessions").insert({
            user_id: body.user_id,
            training_id: body.training_id,
            credit_gain: body.credit_gain,
            time: body.time,
            RPM_avg: body.RPM_avg,
            finished_at: body.finished_at
        });

    }




}