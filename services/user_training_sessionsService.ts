import * as Knex from 'knex';

export class User_Training_SessionsService{
    constructor(private knex:Knex){

    }

    getAllUserTrainingSession(){
        return this.knex.select("*").from('users').innerJoin("user_training_sessions", "user_training_sessions.user_id", "users.id");
    }

    async creatOneTraining(body:any){
        return this.knex.table("user_training_sessions").insert({
            user_id: body.user_id,
            training_id:body.training_id,
            credit_gain:body.credit_gain,
            time:body.time,
            RPM_avg:body.RPM_avg,
            finished_at:body.finished_at
        });

    }


    // async createOneUser(body:any){
    //   const user = await this.knex.select("username")
    //     .from("users")
    //     .where("username", body.username);
    //     console.log(user)
    //   if (user.length>0) {
    //     throw new Error("Your name has been used by someone else");
    //   } else {
    //     return this.knex.table("users").insert({
    //         username: body.username,
    //         password:body.password,
    //         user_code:body.user_code,
    //         isAdmin:body.isAdmin,
    //         credit:body.credit
            
    //     });
    //   }
    // }

    // changeProfile(body:any){
    //     console.log(body);
    //     return this.knex.table("users")
    //             .update(body).where("username", body.username);
    // }

    // delete(username:string){
    //     return this.knex.table("users").where("username",username).delete();
    // }

}