import * as Knex from 'knex';

export class UserService{
    constructor(private knex:Knex){

    }

    getAllUsers(){
        return this.knex.select("*").from('users');
    }

    async createOneUser(body:any){
      const user = await this.knex.select("username")
        .from("users")
        .where("username", body.username);
        console.log(user)
      if (user.length>0) {
        throw new Error("Your name has been used by someone else");
      } else {
        return this.knex.table("users").insert({
            username: body.username,
            password:body.password,
            user_code:body.user_code,
            isAdmin:body.isAdmin,
            credit:body.credit
            
        });
      }
    }

    changeProfile(body:any){
        console.log(body);
        return this.knex.table("users")
                .update(body).where("username", body.username);
    }

    delete(username:string){
        return this.knex.table("users").where("username",username).delete();
    }

}





// async changeProfile(username: string,body:any){
//     console.log(body);
//     const users = await this.knex.select("*").from('users');
//     const user = users.find(function (user: any) {
//         if (user.username === username) {
//             return true;
//         } else {
//             return false;
//         }
//     });
//     if (typeof user === 'undefined') {

//         throw new Error('no_user');
//     }

//     return this.knex.table("users")
//             .update(body).where("username", body.username);
// }