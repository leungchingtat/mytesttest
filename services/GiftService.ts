import * as Knex from 'knex';

export class GiftService{
    constructor(private knex:Knex){

    }

    getGifts(){
        return this.knex.select("*").from('gifts');
    }

    async addGift(body:any,uploadedPath?:string){
      const gift = await this.knex.select("giftname")
        .from("gifts")
        .where("giftname", body.giftname);
      if (gift.length>0) {
        throw new Error("Your gift has been used by someone else");
      } else {
        return this.knex.table("gifts").insert({
            giftname: body.giftname,
            point:body.point,
            image_path:uploadedPath,
            stock:body.stock
            
        });
      }
    }

    updateGift(body:any,uploadedPath?:string){
        if (uploadedPath){
        body.image_path=uploadedPath;}
        return this.knex.table("gifts")
                .update(body).where("id",body.id);
    }

    delete(giftname:string){
        return this.knex.table("gifts").where("giftname",giftname).delete();
    }




    // updateGift(id:number,body:any,uploadedPath?:string){
    //     if (uploadedPath){
    //     body.image_path=uploadedPath;}
    //     return this.knex.table("gifts")
    //             .update(body).where("id",id);
    // }

    // deleteGift(id:number){
    //     return this.knex.table("gifts").where("id",id).delete();
    // }

}
